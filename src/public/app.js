function fetchAndVisualizeData() {
  fetch("./output/wonTossAndMatch.json")
    .then(data => data.json())
    .then(visualizeData);
}

fetchAndVisualizeData();

function visualizeData(data) {
  visualizeWonTossAndMatch(data)
  return;
}

function visualizeWonTossAndMatch(wonTossAndMatch) {
  const seriesData = [];
  for (let name in wonTossAndMatch) {
    for (const team in wonTossAndMatch[name]) {
      seriesData.push([team, wonTossAndMatch[name][team]]);
    }
  }
  console.log(seriesData);
  Highcharts.chart("wonTossAndMatch", {
    chart: {
      type: "column"
    },
    title: {
      text: "1. Won Toss And Match"
    },
    subtitle: {
      text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
    },
    xAxis: {
      type: "category"
    },
    yAxis: {
      min: 0,
      title: {
        text: "Won Toss And Match"
      }
    },
    series: [{
      name: "Count",
      data: seriesData
    }]
  });
}