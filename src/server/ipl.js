const wonTossAndMatch = (matches) => {
    // let obj = {};
    // matches.forEach(match => {
    //     if(match["toss_winner"] === match["winner"]){
    //         if(obj.hasOwnProperty(match["winner"])){
    //             obj[match["winner"]]++
    //         }else {
    //             obj[match["winner"]] = 1; 
    //         }
    //     }
    // });

    // Refactored code
    return matches.filter(match => match["toss_winner"] === match["winner"])
    .reduce((acc, cv) => {
        let winner = cv["winner"];
        acc.hasOwnProperty(winner) ? acc[winner]++ : acc[winner] = 1;
        return acc
    },{})
}


const playerOfMatchEachSeason = (matches) => {
    // let obj = {};
    // matches.forEach(match => {
    //     // check if season exists
    //     if(obj.hasOwnProperty(match["season"])){
    //         if(obj[match["season"]].hasOwnProperty(match["player_of_match"])){
    //             obj[match["season"]][match["player_of_match"]]++
    //         }else {
    //             obj[match["season"]][match["player_of_match"]] = 1;
    //         }
    //     }else {
    //         obj[match["season"]] = {};
    //         obj[match["season"]][match["player_of_match"]] = 1;
    //     }
    // });

    // ****************************************
    //              Refactored code
    // ****************************************

    let playerOfMatch = matches.reduce((acc, cv) => {
        let season = cv["season"];
        let potm = cv["player_of_match"]; //player of the match
        if(acc.hasOwnProperty(season)){
            if(acc[season].hasOwnProperty(potm)){
                acc[season][potm]++
            }else {
                acc[season][potm] = 1;
            }
        }else {
            acc[season] = {};
            acc[season][potm] = 1;
        }
        return acc;
    }, {})

    // convert format to object-array
    let res = {};
    for (let season in playerOfMatch) {
        let arr = []
        for (let player in playerOfMatch[season]) {
            arr.push([player, playerOfMatch[season][player]])
        }
        res[season] = arr
    }

    // sort & get first value -- player of match
    let result = {}
    for (let season in res) {
        res[season].sort((a,b) => b[1] - a[1]);
        result[season] = res[season][0]
    }

    return result
}

const strikeRateOfBatsman = (matches, deliveries, batsman = 'MS Dhoni') => {
    // let seasonData = {};

    // // get ids matching to a season
    // matches.forEach(match => {
    //     if(seasonData.hasOwnProperty(match["season"])){
    //         seasonData[match["season"]].push(match['id']);
    //     }else {
    //         seasonData[match["season"]] = [match['id']];
    //     }
    // })

    // let seasons = Object.keys(seasonData);
    // let ids = Object.values(seasonData);

    // // get total runs & total balls faced per season
    // let result = {};
    // seasons.map(s => {
    //     result[s] = {};
    //     result[s]['totalRuns'] = 0;
    //     result[s]['totalBalls'] = 0;
    //     result[s]['extraBalls'] = 0;
    //     result[s]['ballsFaced'] = 0;

    //     deliveries.forEach( d => {
    //         if(seasonData[s].includes(d['match_id']) && d['batsman'] === batsman ){
    //             result[s]['totalRuns'] += Number(d['batsman_runs']); 
    //             result[s]['totalBalls']++; 
    //         }
    //         if( seasonData[s].includes(d['match_id']) && d['batsman'] === batsman){
    //             if(d['wide_runs'] !== '0'){
    //                 result[s]['extraBalls']++; 
    //             } 
    //         }
    //     })
    //     result[s]['ballsFaced'] = Number(result[s]['totalBalls']) - Number(result[s]['extraBalls']);
    //     result[s] = (( Number(result[s]['totalRuns']) / Number(result[s]['ballsFaced']) ) * 100).toFixed(2) ;
        

    //     delete result[s]['totalRuns'];
    //     delete result[s]['totalBalls'];
    //     delete result[s]['extraBalls'];
    //     delete result[s]['ballsFaced'];
    // })

    // ****************************************
    //              Refactored code
    // ****************************************
    

    // get ids for seasons
    let seasonIds = matches.reduce((acc, cv) => {
        let season = cv["season"];
        let id = cv["id"];
        if(acc.hasOwnProperty(season)){
            acc[season].push(id);
        }else {
            acc[season] = [id];
        }
        return acc
    },{})

    // batsman data for each season
    let seasonData = {}
    for(const season in seasonIds) {
        let batsmanData = {}
        batsmanData['totalRuns'] = 0;
        batsmanData['totalBalls'] = 0;
        batsmanData['extraBalls'] = 0;
        batsmanData['ballsFaced'] = 0;
        
        deliveries.filter(d => seasonIds[season].includes(d['match_id']) && d['batsman'] === batsman )
            .reduce((acc, cv) => {
                batsmanData['totalRuns'] += Number(cv['batsman_runs']); 
                batsmanData['totalBalls']++;
                if(cv['wide_runs'] !== '0'){
                    batsmanData['extraBalls']++; 
                } 
            })

        batsmanData['ballsFaced'] = Number(batsmanData['totalBalls']) - Number(batsmanData['extraBalls']);
        batsmanData = (( Number(batsmanData['totalRuns']) / Number(batsmanData['ballsFaced']) ) * 100).toFixed(2) ;
        seasonData[season] = batsmanData
    }

    return {[batsman] : seasonData};
}


const highestDismissal = (deliveries, batsman = 'MS Dhoni') => {
    // highest no of times one player is dismissed by another 
    // console.log(deliveries.filter(d => d['player_dismissed'] && d['player_dismissed'] !== 'run out'));
    let res = {};
    deliveries.forEach(d => {
        if(d['player_dismissed'] && d['player_dismissed'] !== 'run out'){
            if(res.hasOwnProperty(d['batsman'])){
                if(res[d['batsman']].hasOwnProperty(d['bowler'])){
                    res[d['batsman']][d['bowler']]++;
                }else {
                    res[d['batsman']][d['bowler']] = 1;
                }
            } else {
                res[d['batsman']] = {};
                res[d['batsman']][d['bowler']] = 1;
            }
        }
    })

    let data = {}
    for (let batsman in res) {
        let arr = [];
        for (const bowler in res[batsman]) {
            arr.push([bowler, res[batsman][bowler]]);
        }
        data[batsman] = arr;
        data[batsman].sort((a,b) => b[1] - a[1]);
    }

    let result = {}
    for (let batsman in data) {
        result[batsman] = data[batsman][0];
    }

    return {[batsman] : result[batsman]}
}


const mostEconomicBowlersInSuperOvers = (deliveries) => {
    let result = {};
    let balls = {};

    for (let delivery of deliveries) {
        const isSuperOver = delivery.is_super_over;
        if (isSuperOver === '1') {
            const bowler = delivery.bowler;
            const totalRuns = Number(delivery.total_runs);
            const wideRuns = Number(delivery.wide_runs);
            const legbyeRuns = Number(delivery.legbye_runs);
            const noBallRuns = Number(delivery.noball_runs);

            // If result object  or balls object has delivery bowler as key 
            // add the total runs and balls else add the key with total runs and ball
            if (balls[bowler] || result[bowler]) {
                result[bowler] += totalRuns;
                if (wideRuns === 0 && legbyeRuns === 0 && noBallRuns === 0)
                    balls[bowler] += 1;
                else
                    balls[bowler] += 0;
            } else {
                result[bowler] = totalRuns;
                if (wideRuns < 1 && legbyeRuns < 1 && noBallRuns < 1)
                    balls[bowler] = 1;
                else
                    balls[bowler] = 0;
            }
        }
    }

    // Calculate the economy rate of bowler by dividing total runs given by 
    // total number of overs
    for (let runs in result) {
        if (result.hasOwnProperty(runs)) {
            result[runs] = Number((result[runs] / Number((balls[runs] / 6).toFixed(2))).toFixed(2));
        }
    }

    // Sort the result for top 10 economy bowlers
    let entries = Object.entries(result);
    result = entries.sort((a, b) => b[1] - a[1]);
    // console.log(result);

    let topTenSuperOver = {}

    result.forEach(bowler => {
        topTenSuperOver[bowler[0]] = bowler[1]
    })


    return topTenSuperOver ;
}

module.exports = { 
    wonTossAndMatch,
    playerOfMatchEachSeason,
    strikeRateOfBatsman,
    highestDismissal,
    mostEconomicBowlersInSuperOvers
}