const fs = require('fs');
const path = require('path');
const csv = require('csvtojson');
const express = require('express')
const app = express()

const home = path.join(__dirname, "../public/index.html");
const DELIVERIES_FILE_PATH = path.join(__dirname, "../data/deliveries.csv");
const MATCHES_FILE_PATH = path.join(__dirname, "../data/matches.csv");
const JSON_OUTPUT_FILE_PATH = path.join(__dirname, "../public/output/");

const { wonTossAndMatch, 
        playerOfMatchEachSeason, 
        strikeRateOfBatsman,
        highestDismissal, 
        mostEconomicBowlersInSuperOvers } = require('./ipl');

function main() {
    
    csv()
    .fromFile(MATCHES_FILE_PATH)
    .then((matches)=>{
        csv()
        .fromFile(DELIVERIES_FILE_PATH)
        .then((deliveries)=>{
            let result = [];
            result.push({wonTossAndMatch: wonTossAndMatch(matches)})
            result.push({playerOfMatchEachSeason: playerOfMatchEachSeason(matches)})
            result.push({strikeRateOfBatsman: strikeRateOfBatsman(matches, deliveries)});
            result.push({highestDismissal: highestDismissal(deliveries)});
            result.push({mostEconomicBowlersInSuperOvers: mostEconomicBowlersInSuperOvers(deliveries)});

            saveData( result[0], 'wonTossAndMatch');
            saveData( result[1], 'playerOfMatchEachSeason');
            saveData( result[2], 'strikeRateOfBatsman');
            saveData( result[3], 'highestDismissal');
            saveData( result[4], 'mostEconomicBowlersInSuperOvers');
        })
    })
}

main();

const saveData = (result, fileName = 'data') => {    
    const jsonString = JSON.stringify(result);
    fs.writeFile(JSON_OUTPUT_FILE_PATH + fileName +".json", jsonString, "utf8", err => {
        if (err) {
        console.error(err);
        }
    })   
}

app.use(express.static("public"));

// serve index.html 
app.get('/', function(req, res){
  res.sendFile(home), {data : "this is the data.."};
});

const PORT = process.env.PORT || 8000;

app.listen(PORT, () => {
    console.log(`Server running at : ${PORT}`);
})

 
